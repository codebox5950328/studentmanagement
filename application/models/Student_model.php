<?php 
   Class Student_model extends CI_Model {

   public function add($data){
         
      $this->db->insert('students',$data);
   }

   public function update($editstudent,$id){
      
      $this->db->WHERE('id',$id);
      $this->db->update("students",$editstudent);

   }

   public function get_students(){
      
      $query = $this->db->SELECT('*')->GET('students');
      return $query->result_array();
      
   }

   public function get_student($id){
      
      $query = $this->db->GET_WHERE('students',array('id'=>$id));
      return $query->result_array();
      
   }

   public function delete($id){
      
      $this->db->delete('students', array('id' => $id));
   }
}
