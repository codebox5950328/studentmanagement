<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Student Table</title>
</head>
<body>

    <h1><u>Edit and Update Student</u></h1>

    <?php foreach($student as $edit): ?>

    <form  action="<?= site_url('edit/'.$edit['id']) ?>" method="POST">

    <?php echo validation_errors(); ?> 

    <table>
    <tr>
    <th>Name:</th>
    <td><input type="text" name="name"  placeholder="name"  value="<?php echo $edit["name"];?>"></td>
    </tr>

    <tr>
    <th>Gender:</th>
    <td><input type="text" name="gender" placeholder="gender" value="<?php echo $edit["gender"];?>"></td>
    </tr>

    <tr>
    <th>Age:</th>
    <td><input type="text" name="age" placeholder="age" value="<?php echo $edit["age"];?>"></td>
    </tr>

    <tr>
    <th>School Name:</th>
    <td><input type="text" name="school_name" placeholder="school name" value="<?php echo $edit["school_name"];?>"></td>
    </tr>

    <tr>
    <th>University:</th>
    <td><input type="text" name="university" placeholder="university" value="<?php echo $edit["university"];?>"></td>
    </tr>

    <?php endforeach; ?>
    </table><br>

    <input type="submit" value="Update" name="add">
</form>



