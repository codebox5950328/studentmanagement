<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Student Table</title>
</head>
<body>

   <h1><u>Add Student</u></h1>
   
<form action="<?php echo site_url('student/add') ?>" method="POST" >
    <?php echo validation_errors(); ?> 
    <table cellpadding="4" cellspacing="0">
            <tr>  
                <th>Name:</th>  
                <td><input type="text" name="name" value="<?php echo set_value('name') ?>"></td>
            </tr>

            <tr>  
                <th>Gender:</th> 
                <td><input type="text" name="gender" value="<?php echo set_value('gender') ?>"></td>
            </tr>

            <tr>  
                <th>Age:</th>   
                <td><input type="number" name="age" value="<?php echo set_value('age') ?>"></td>
            </tr>

            <tr>  
                <th>School name:</th>   
                <td><input type="text" name="school_name" value="<?php echo set_value('school') ?>"></td>
                
            </tr>

            <tr>  
                <th>University:</th>  
                <td><input type="text" name="university" value="<?php echo set_value('university') ?>"></td>
                
            </tr> 
            </table>
                <br><input type="submit" value="Add">  
    </form><br>
         

</body>
</html>


