<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Student Table</title>
</head>
<body>

     <h1><u>Student Details</u></h1>
<table border="1" cellspacing="0" cellpadding="5">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Gender</th>
        <th>Age</th>
        <th>School Name</th>
        <th>University</th>
        <th>Actions</th>
</tr>

<?php foreach($students as $student): ?>
<tr>
    <td><?php echo $student["id"];?></td>

    <td><?php echo $student["name"];?></td>

    <td><?php echo $student["gender"];?></td>

    <td><?php echo $student["age"];?></td>

    <td><?php echo $student["school_name"];?></td>

    <td><?php echo $student["university"];?></td>

    <td> 
    <a href=<?= site_url('edit/'.$student["id"]) ?>>Edit</a> |
    <a href=<?= site_url('remove/'.$student["id"]) ?>>Delete</a>
    </td>

    <?php endforeach; ?>
</tr>
</table>

<br><a href="<?php echo site_url('student/add') ?>"><button>Add Student</button></a>
</body>
</html>
