<?php
        class Student extends CI_Controller{
           
            
            public function index(){
                $data['students']=$this->Student_model->get_students();
                $this->load->view('index',$data);
            }
            
            public function add(){
                $this->form_validation->set_rules("name", "Name", "required|regex_match[/[a-zA-Z]|\s/]");
                $this->form_validation->set_rules("gender", "Gender", "required|alpha");
                $this->form_validation->set_rules("age", "Age", "required|numeric");  
                $this->form_validation->set_rules("school_name","School Name", "required|regex_match[/^([a-z ])+$/i]");
                $this->form_validation->set_rules("university", "University", "required|regex_match[/[a-zA-Z]|\s/]");
                
                if ($this->form_validation->run()==FALSE){ 
                   $this->load->view('add');
                } 
                else{ 
                    $data = array(
                        'name'=> $this->input->post('name'),
                        'gender'=> $this->input->post('gender'),
                        'age'=> $this->input->post('age'),
                        'school_name'=> $this->input->post('school_name'),
                        'university' =>$this->input->post('university'),
                    );
                    $this->Student_model->add($data);
                    redirect('student');
					exit;
                } 
            }
            

            public function edit($id){
                
                $this->form_validation->set_rules("name", "Name", "required|regex_match[/[a-zA-Z]|\s/]");
                $this->form_validation->set_rules("gender", "Gender", "required|alpha");
                $this->form_validation->set_rules("age", "Age", "required|numeric");
                $this->form_validation->set_rules("school_name","School Name", 'required|regex_match[/[a-zA-Z]|\s/]');
                $this->form_validation->set_rules("university", "University", "required|regex_match[/[a-zA-Z]|\s/]");
                
                if ($this->form_validation->run()==False){ 
					$data['student']= $this->Student_model->get_student($id);
                    $this->load->view('edit',$data);
                } 
                else { 
                    $editstudent = array(
                        'name'=> $this->input->post('name'),
                        'gender'=> $this->input->post('gender'),
                        'age'=> $this->input->post('age'),
                        'school_name'=> $this->input->post('school_name'),
                        'university' =>$this->input->post('university'),
                    );
                    $this->Student_model->update($editstudent,$id);
                    redirect('student');
                    exit;
                } 
            }
            public function remove($id){
                $this->Student_model->delete($id);
                redirect('student');
                exit;
            }
}
?>

